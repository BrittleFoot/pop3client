package org.bitbucket.brittlefoot.pop3.client.mime;

import javafx.util.Pair;
import org.bitbucket.brittlefoot.pop3.Util;

import java.util.HashMap;

class MimeMessage {

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding";
    public final String body;
    public final ContentType contentType;
    public final ContentTransferEncoding encodeing;
    private final HashMap<String, String> header;

    public MimeMessage(String raw) {
        Pair<HashMap<String, String>, String> parseMime = Util.parseMime(raw);
        header = parseMime.getKey();
        body = parseMime.getValue();
        String ctype = header.getOrDefault(CONTENT_TYPE, "");
        contentType = ctype.isEmpty() ? ContentType.getDefault() : new ContentType(ctype);
        String enc = header.getOrDefault(CONTENT_TRANSFER_ENCODING, "7bit");
        encodeing = new ContentTransferEncoding(enc);

    }

}
