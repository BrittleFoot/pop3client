package org.bitbucket.brittlefoot.pop3.client;

import java.util.Objects;

public class Letter {

    public final LetterBody body;
    private final String uidl;
    private final Integer size;
    public Integer num;

    public Letter(String uidl, Integer size) {
        this.uidl = uidl;
        this.size = size;
        this.body = new LetterBody();
    }

    @Override
    public String toString() {
        if (body.getHeaders().isEmpty()) {
            return "Letter(uidl=" + uidl + ", size=" + size + ')';
        }
        else {
            MainHeader header = body.getParsedHeader();
            return header.getSubj() + " <" + header.getFrom().trim() + "> (" + size + " bytes)";
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.uidl);
        hash = 13 * hash + Objects.hashCode(this.size);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Letter other = (Letter) obj;
        return Objects.equals(this.uidl, other.uidl) && Objects.equals(this.size, other.size);
    }

}
