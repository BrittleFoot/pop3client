package org.bitbucket.brittlefoot.pop3.client.mime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContentType {

    private static final Pattern KEY_PATTERN = Pattern.compile("(?<key>[^\\s=,]*)=(?<value>[^\\s,]*)");
    public final String type;
    public final String subtype;
    public final HashMap<String, String> args;
    private final ArrayList<String> listArgs;

    public ContentType(String value) {
        args = new HashMap<>();
        listArgs = new ArrayList<>();

        if (value == null || value.isEmpty()) {
            type = subtype = "";
        }
        else {
            String[] split = value.split(";");
            String types = split[0].trim();
            if (types.contains("/")) {
                String[] splittedTypes = types.split("/");
                type = splittedTypes[0];
                subtype = splittedTypes[1];
            }
            else {
                type = types;
                subtype = "";
            }

            if (split.length <= 1) {
                return;
            }

            for (int i = 1; i < split.length; i++) {
                String record = split[i].trim();
                if (!record.isEmpty()) {
                    Matcher matcher = KEY_PATTERN.matcher(record);
                    if (matcher.find()) {
                        args.put(record.substring(matcher.start("key"), matcher.end("key")).trim(),
                                record.substring(matcher.start("value"), matcher.end("value")).trim());
                    }
                    else {
                        listArgs.add(record);
                    }
                }

            }

        }
    }

    public static ContentType getDefault() {
        return new ContentType("text/plain; charset=us-ascii");
    }

}
