package org.bitbucket.brittlefoot.pop3.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.bitbucket.brittlefoot.pop3.POP3;
import org.bitbucket.brittlefoot.pop3.POPAnswer;
import org.bitbucket.brittlefoot.pop3.PopAlreadyConnected;
import org.bitbucket.brittlefoot.pop3.PopNotConnected;
import org.bitbucket.brittlefoot.pop3.client.events.ConnectionRequired;
import org.bitbucket.brittlefoot.pop3.client.events.LoadLetterList;
import org.bitbucket.brittlefoot.pop3.client.events.UpdateTitle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class POP3Client extends Application {

    private HashMap<Letter, LetterBox> nodes;
    private ObservableList<Letter> items;
    private POP3 pop;
    private LoginInfo auth;
    private ListView<Letter> list;
    private UseTls usetls = UseTls.AUTO;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private String genTitle() {
        return String.join(" ", new String[]
                {
                        "POP3",
                        pop == null ? "" : (pop.domain + ":" + pop.port),
                        auth == null ? "" : auth.login
                });
    }

    POP3 getPop() {
        return pop;
    }

    ObservableList<Letter> getItems() {
        return items;
    }

    ListView<Letter> getList() {
        return list;
    }

    @Override
    public void start(Stage primaryStage) {
        pop = null;
        auth = null;

        LabelMenu connectMenu = new LabelMenu("Set server", e -> setserver(primaryStage));
        LabelMenu loginMenu = new LabelMenu("Log in", e -> login(primaryStage));
        LabelMenu exitMenu = new LabelMenu("Log out", e -> quit(primaryStage));

        items = FXCollections.observableArrayList();
        list = new ListView<>(items);
        list.setPrefHeight(5000);
        list.setMinWidth(300);
        list.setPrefWidth(300);

        VBox dataBox = new VBox();

        nodes = new HashMap<>();

        list.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        dataBox.getChildren().clear();
                        LetterBox box = nodes.get(newValue);
                        if (box == null) {
                            Show.warning("Letter not found!",
                                    newValue.toString() + "Not found in map!");
                            return;
                        }

                        dataBox.getChildren().add(box);
                        box.sendLoadEvent(false);
                    }

                });

        HBox hBox = new HBox();
        hBox.getChildren().addAll(list, dataBox);
        hBox.setPadding(new Insets(4, 4, 4, 4));
        hBox.setSpacing(4);

        VBox root = new VBox();
        root.getChildren().addAll(new MenuBar(connectMenu, loginMenu, exitMenu), hBox);

        primaryStage.addEventHandler(UpdateTitle.eType, e -> primaryStage.setTitle(genTitle()));
        primaryStage.addEventHandler(ConnectionRequired.eType, e -> reconnect(primaryStage));
        primaryStage.addEventFilter(LoadLetterList.eType, e -> loadLetterList(primaryStage));
        primaryStage.setOnCloseRequest(e -> quit(primaryStage));

        UpdateTitle.fireFrom(primaryStage);

        primaryStage.setScene(new Scene(root, 1024, 768));
        primaryStage.show();
    }

    private void reconnect(Stage primaryStage) {
        if (pop == null) {
            Show.warning("No pop server specified", "Use 'set server' first");
        }
        else if (auth == null) {
            Show.warning("Not autorizated user", "Set you login & password");
        }
        else {
            try {
                POPAnswer connect;
                if (usetls == UseTls.AUTO) {
                    connect = pop.connect();
                }
                else {
                    connect = pop.connect(usetls == UseTls.YES);
                }

                if (!connect.succ) {
                    Show.warning("Protocol error!", connect.message);
                    return;
                }
                POPAnswer user = pop.user(auth.login);
                if (!user.succ) {
                    Show.warning("Protocol error!", user.message);
                    return;
                }
                POPAnswer pass = pop.pass(auth.pass);
                if (!pass.succ) {
                    Show.warning("Protocol error!", pass.message);
                    return;
                }

                LoadLetterList.fireFrom(primaryStage);
            }
            catch (PopAlreadyConnected ex) {
                Show.warning("Already connected!", "");
            }
            catch (PopNotConnected ex) {
                Show.warning("Connection fail", ex.getMessage());
            }

        }
    }

    private void quit(Stage primaryStage) {

        if (pop != null) {
            try {
                pop.quit();
            }
            catch (PopNotConnected ex) {
                // dafaq
            }
            finally {
                pop = null;
                auth = null;
                UpdateTitle.fireFrom(primaryStage);
            }
        }

    }

    private void setserver(Stage primaryStage) {
        Show.inputServer("localhost:110").ifPresent(pvalue ->
        {
            quit(primaryStage);

            String value = pvalue.getKey();
            usetls = pvalue.getValue();
            String[] split = value.split(":");
            if (split.length == 2) {
                try {
                    pop = new POP3(split[0], Integer.parseInt(split[1]));
                    UpdateTitle.fireFrom(primaryStage);

                }
                catch (Exception ex) {
                    Show.warning("Unexpected", ex.getLocalizedMessage());
                }

            }
            else {
                Show.warning("Something goes wrong.",
                        "Be careful with host:port format!");
            }
        });
    }

    private void login(Stage primaryStage) {
        if (auth != null) {
            Optional<ButtonType> ack = Show.ack("Authentification account already exist: "
                    + auth.login + ". Replace it?");

            if (ack.isPresent() && ack.get() != ButtonType.OK) {
                ConnectionRequired.fireFrom(primaryStage);
                return;
            }
        }

        Show.loginDialog("Input your post login and password")
                .ifPresent(loginPair ->
                {
                    auth = new LoginInfo(loginPair.getKey(), loginPair.getValue());

                    UpdateTitle.fireFrom(primaryStage);
                    ConnectionRequired.fireFrom(primaryStage);

                });
    }

    private void loadLetterList(Stage primaryStage) {
        Thread thread = new Thread(() ->
        {
            ArrayList<Integer> sizes = new ArrayList<>();
            ArrayList<Integer> numbers = new ArrayList<>();
            ArrayList<String> uidls = new ArrayList<>();

            POPAnswer lista = null;
            POPAnswer uidla = null;

            try {
                lista = pop.list();
                if (!Show.good(lista)) {
                    return;
                }
                uidla = pop.uidl();
                if (!Show.good(uidla)) {
                    return;
                }

            }
            catch (PopNotConnected ex) {
                ConnectionRequired.fireFrom(primaryStage);
            }

            for (String pare : lista.getAdditional().split("\n")) {
                String[] split = pare.trim().split(" ");
                try {
                    Integer num = Integer.parseInt(split[0]);
                    Integer size = Integer.parseInt(split[1]);
                    sizes.add(size);
                    numbers.add(num);
                }
                catch (NumberFormatException |
                        IndexOutOfBoundsException ex) {
                    System.err.println("Not expected in info record: " + pare);
                }
            }

            for (String pare : uidla.getAdditional().split("\n")) {
                String[] split = pare.trim().split(" ");
                try {
                    String uidl = split[1];
                    uidls.add(uidl);
                }
                catch (NumberFormatException |
                        IndexOutOfBoundsException ex) {
                    System.err.println("Not expected in uidl record: " + pare);
                }
            }

            ArrayList<Letter> letters = new ArrayList<>();
            for (int i = 0; i < numbers.size(); i++) {
                Letter letter = new Letter(uidls.get(i), sizes.get(i));
                letter.num = numbers.get(i);
                if (!nodes.containsKey(letter)) {
                    letters.add(letter);
                }
                else {
                    letters.add(nodes.get(letter).letter);
                }
            }

            Platform.runLater(() ->
            {
                HashMap<Letter, LetterBox> tmp = new HashMap<>();
                letters.forEach(letter ->
                {
                    if (nodes.containsKey(letter)) {
                        tmp.put(letter, nodes.get(letter));
                    }
                    else {
                        tmp.put(letter, new LetterBox(primaryStage, this, letter));
                    }
                });
                nodes = tmp;

                items.clear();
                items.addAll(letters);
                list.setItems(items);
            });
        });
        thread.setDaemon(true);
        thread.start();
    }

}
