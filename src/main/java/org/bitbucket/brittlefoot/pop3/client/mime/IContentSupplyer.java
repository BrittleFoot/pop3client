package org.bitbucket.brittlefoot.pop3.client.mime;

import java.util.Collection;

public interface IContentSupplyer {

    Collection<ContentNode> getContent();
}
