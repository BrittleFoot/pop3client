package org.bitbucket.brittlefoot.pop3.client;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.bitbucket.brittlefoot.pop3.POP3;
import org.bitbucket.brittlefoot.pop3.POPAnswer;
import org.bitbucket.brittlefoot.pop3.PopNotConnected;
import org.bitbucket.brittlefoot.pop3.client.events.ConnectionRequired;
import org.bitbucket.brittlefoot.pop3.client.mime.ContentNode;

import java.util.Collection;
import java.util.function.Consumer;

class LetterBox extends VBox {

    private static final Image girl = new Image(LetterBox.class.getClassLoader().getResourceAsStream("girl.jpg"));

    public final Letter letter;
    private final Stage root;
    private final POP3Client client;
    private final TextArea textArea;
    private final Button loadButton;
    private boolean loaded = false;

    public LetterBox(Stage root, POP3Client client, Letter letter) {
        super();
        this.root = root;
        this.client = client;
        this.letter = letter;
        loadButton = new Button("Load message");

        loadButton.setPrefSize(200, 100);
        textArea = new TextArea();
        textArea.setPrefSize(1000000, 1000000);

        loadButton.setOnMousePressed(e ->
        {
            this.getChildren().clear();
            Label label = new Label();
            ImageView iv = new ImageView(girl);
            iv.setOnMousePressed(e_ -> sendLoadEvent(true));
            label.setGraphic(iv);
            label.setPrefSize(1000000, 1000000);
            this.getChildren().add(label);

            Thread loadthread = new Thread(() ->
            {
                try {
                    loadLetter();
                }
                catch (Exception ex) {
                    Platform.runLater(() ->
                    {
                        this.getChildren().clear();
                        loadButton.setText("Reload message");
                        this.getChildren().add(loadButton);
                        Show.warning("Exception", ex.getMessage());
                    });
                    ex.printStackTrace();
                    return;
                }

                Platform.runLater(() ->
                {
                    this.getChildren().clear();
                    packLetter();
                });
            });
            loadthread.setDaemon(true);
            loadthread.start();
        });

        this.getChildren().add(loadButton);

        startSafePopNewThread(pop ->
        {
            try {
                POPAnswer top = pop.top(letter.num, 0);
                if (Show.good(top)) {
                    letter.body.setHeaders(top.getAdditional());
                    client.getList().refresh();
                }
            }
            catch (PopNotConnected ex) {
                ConnectionRequired.fireFrom(root);
            }
        });

    }

    private static MouseEvent getMouseEvent() {
        return new MouseEvent(MouseEvent.MOUSE_PRESSED,
                0, 0, 0, 0, MouseButton.NONE, 0,
                true, true, true, true, true, true,
                true, true, true, true, null);
    }

    public void sendLoadEvent(boolean force) {
        if (!loaded || force) {
            MouseEvent mouseEvent = getMouseEvent();
            loadButton.fireEvent(mouseEvent);
        }
        loaded = true;
    }

    private void loadLetter() {

        String messagebody = null;

        POP3 pop = client.getPop();
        if (pop != null) {
            try {
                POPAnswer retr = pop.retr(letter.num);
                if (!Show.good(retr)) {
                    return;
                }

                messagebody = retr.getAdditional();

            }
            catch (PopNotConnected ex) {
                ConnectionRequired.fireFrom(root);
            }
        }

        if (messagebody == null) {
            return;
        }

        letter.body.setText(messagebody);

    }

    private void packLetter() {
        this.getChildren().add(
                new MenuBar(
                        new LabelMenu(
                                "Delete",
                                e -> startSafePopNewThread(pop ->
                                {
                                    try {
                                        POPAnswer dele = pop.dele(letter.num);
                                        if (Show.good(dele)) {
                                            Platform.runLater(() -> client.getItems().remove(letter));
                                        }
                                    }
                                    catch (PopNotConnected ex) {
                                        ConnectionRequired.fireFrom(root);
                                    }
                                })), new LabelMenu("Reload", ef -> sendLoadEvent(true))));

        if (letter.body.getParsedHeader() != null) {
            MainHeader head = letter.body.getParsedHeader();
            TextField from = new TextField("From: " + head.getFrom());
            TextField subj = new TextField("Subject: " + head.getSubj());
            from.setEditable(false);
            subj.setEditable(false);

            this.getChildren().addAll(from, subj);
        }

        if (!letter.body.getHeaders().isEmpty()) {
            TextArea tf = new TextArea(letter.body.getHeaders());
            tf.setPrefSize(1000, 300);
            TitledPane pane = new TitledPane("Headers", tf);
            pane.expandedProperty().setValue(false);
            this.getChildren().add(pane);
        }

        if (!letter.body.getText().isEmpty()) {
            textArea.setText(letter.body.getText());
            TitledPane pane = new TitledPane("Raw message", textArea);
            pane.expandedProperty().setValue(false);
            this.getChildren().add(pane);
        }

        Collection<ContentNode> content = letter.body.getContent();
        if (content != null) {
            content.forEach(node -> this.getChildren().add(new TitledPane(node.name, node.node)));
        }

    }

    private void startSafePopNewThread(Consumer<POP3> func) {
        Thread t = new Thread(() ->
        {
            POP3 pop = client.getPop();
            if (pop != null) {
                func.accept(pop);
            }

        });
        t.setDaemon(true);
        t.start();
    }
}
