package org.bitbucket.brittlefoot.pop3.client;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.input.MouseEvent;


class LabelMenu extends Menu {

    public LabelMenu(String titile, EventHandler<? super MouseEvent> cickCallback) {
        Label label = new Label(titile);
        label.setOnMousePressed(cickCallback);
        setGraphic(label);
    }

}
