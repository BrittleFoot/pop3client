package org.bitbucket.brittlefoot.pop3.client;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import org.bitbucket.brittlefoot.pop3.POPAnswer;

import java.util.Optional;

class Show {

    public static boolean good(POPAnswer answer) {
        if (!answer.succ) {
            Platform.runLater(() -> Show.warning("Protocol error", answer.message));
            return false;
        }
        return true;
    }

    public static void warning(String shortDescr, String cause) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText(shortDescr);
        alert.setContentText(cause);

        alert.showAndWait();
    }

    public static Optional<ButtonType> ack(String question) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(question);
        alert.setContentText("Are you ok with this?");

        return alert.showAndWait();
    }

    public static Optional<Pair<String, UseTls>> inputServer(String defaultServer) {
        Dialog<Pair<String, UseTls>> dialog = new Dialog<>();
        dialog.setTitle("Login");
        dialog.setHeaderText("I want to connect connect to...");

        ButtonType loginButtonType = new ButtonType("Apply", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        if (!defaultServer.isEmpty()) {
            username.setText(defaultServer);
        }
        username.setPromptText("server:port");
        ChoiceBox<UseTls> tlschoises = new ChoiceBox<>();
        tlschoises.getItems().addAll(UseTls.AUTO, UseTls.YES, UseTls.NO);
        tlschoises.getSelectionModel().select(0);

        grid.add(new Label("Server:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("use tls?"), 0, 1);
        grid.add(tlschoises, 1, 1);

        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(defaultServer.isEmpty());

        username.textProperty().addListener((observable, oldValue, newValue) ->
                loginButton.setDisable(newValue.trim().isEmpty()));

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(username::requestFocus);

        dialog.setResultConverter(dialogButton ->
        {
            if (dialogButton == loginButtonType) {
                return new Pair<>(username.getText(),
                        tlschoises.getSelectionModel().getSelectedItem());
            }
            return null;
        });

        return dialog.showAndWait();
    }

    public static Optional<Pair<String, String>> loginDialog(String prompt) {
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Login");
        dialog.setHeaderText(prompt);

        ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");

        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);

        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        username.textProperty().addListener((observable, oldValue, newValue) ->
                loginButton.setDisable(newValue.trim().isEmpty()));

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(username::requestFocus);

        dialog.setResultConverter(dialogButton ->
        {
            if (dialogButton == loginButtonType) {
                return new Pair<>(username.getText(), password.getText());
            }
            return null;
        });

        return dialog.showAndWait();

    }
}
