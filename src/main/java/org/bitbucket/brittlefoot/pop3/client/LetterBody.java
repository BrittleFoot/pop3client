package org.bitbucket.brittlefoot.pop3.client;

import org.bitbucket.brittlefoot.pop3.client.mime.ContentNode;
import org.bitbucket.brittlefoot.pop3.client.mime.IContentSupplyer;
import org.bitbucket.brittlefoot.pop3.client.mime.MimeContent;

import java.util.Collection;


public class LetterBody implements IContentSupplyer {

    private String text;
    private String headers;
    private MainHeader header;
    private MimeContent content;

    public LetterBody() {
        this.text = "";
        this.headers = "";
        this.content = null;
    }

    public String getText() {
        return text;
    }

    public void setText(String messagebody) {
        content = MimeContent.get(messagebody);
        text = messagebody;
    }

    public String getHeaders() {
        return headers;
    }


    public void setHeaders(String additional) {
        header = new MainHeader(additional);
        headers = additional;
    }

    MainHeader getParsedHeader() {
        return header;
    }

    @Override
    public Collection<ContentNode> getContent() {
        if (content == null)
            return null;

        return content.getContent();
    }

}
