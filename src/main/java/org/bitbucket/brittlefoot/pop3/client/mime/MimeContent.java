package org.bitbucket.brittlefoot.pop3.client.mime;

public abstract class MimeContent implements IContentSupplyer {

    MimeContent(MimeMessage msg) {
    }

    public static MimeContent get(String text) {
        MimeMessage mimeMessage = new MimeMessage(text);

        String type = mimeMessage.contentType.type;

        if ("multipart".equals(type)) {
            return new ContentMultipart(mimeMessage);
        }
        else {
            return new SinglepartContent(mimeMessage);
        }
    }

}
