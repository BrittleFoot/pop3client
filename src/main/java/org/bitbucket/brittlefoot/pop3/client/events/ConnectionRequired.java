package org.bitbucket.brittlefoot.pop3.client.events;

import javafx.event.Event;
import javafx.event.EventType;
import javafx.stage.Stage;

public class ConnectionRequired extends Event {

    public static final EventType<ConnectionRequired> eType
            = new EventType<>("ConnectionRequired");

    private ConnectionRequired(EventType<? extends Event> eventType) {
        super(eventType);
    }

    public static void fireFrom(Stage n) {
        n.fireEvent(new ConnectionRequired(eType));
    }
}
