package org.bitbucket.brittlefoot.pop3.client.mime;

import javafx.scene.Node;


public class ContentNode {

    public final Node node;
    public final String name;

    public ContentNode(Node node, String name) {
        this.node = node;
        this.name = name;
    }

}
