package org.bitbucket.brittlefoot.pop3.client.mime;

import javafx.scene.control.TextArea;

import java.util.ArrayList;
import java.util.Collection;

public class SinglepartContent extends MimeContent {

    private final ArrayList<ContentNode> content = new ArrayList<>();

    public SinglepartContent(MimeMessage msg) {
        super(msg);
        String name = msg.contentType.type + "/" + msg.contentType.subtype;
        name += " " + msg.encodeing.mecanizm;

        content.add(new ContentNode(new TextArea(msg.body), name));

    }

    @Override
    public Collection<ContentNode> getContent() {
        return content;
    }

}
