package org.bitbucket.brittlefoot.pop3.client;

import org.bitbucket.brittlefoot.pop3.Util;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class MainHeader {

    private static final Pattern quotedWordPattern = Pattern.compile("=\\?([^\\s]*\\?){3}?=");
    private final HashMap<String, String> dict;

    public MainHeader(String headers) {
        Predicate<String> pattern = Pattern.compile("^\\s").asPredicate();

        dict = new HashMap<>();

        String lastkey = "";
        for (String line : headers.split("\n")) {
            if (pattern.test(line) && !lastkey.isEmpty()) {
                dict.put(lastkey, dict.get(lastkey) + " " + line.trim());
            }
            else if (line.contains(":")) {
                int sepindex = line.indexOf(":");
                lastkey = line.substring(0, sepindex);
                String value = line.substring(sepindex + 1);
                dict.put(lastkey, value);
            }
        }

        decodeKey("Subject");
        decodeKey("From");

    }

    private void decodeKey(String key) {
        if (dict.containsKey(key)) {
            String value = dict.get(key);
            String replaced = value;

            Matcher matcher = quotedWordPattern.matcher(value);
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();

                String substring = value.substring(start, end);
                String decoded = substring;
                try {
                    decoded = Util.decodeEncodedWord(substring);
                }
                catch (UnsupportedEncodingException ex) {
                    System.err.println("cant decode " + ex.getMessage());
                }
                replaced = replaced.replace(substring, decoded);
            }

            dict.put(key, replaced);
        }
    }

    public String getSubj() {

        return dict.getOrDefault("Subject", "With no subject");
    }

    public String getFrom() {
        return dict.getOrDefault("From", "UNDEFINDED SENDER");
    }
}
