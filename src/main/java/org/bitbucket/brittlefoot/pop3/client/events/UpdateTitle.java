package org.bitbucket.brittlefoot.pop3.client.events;

import javafx.event.Event;
import javafx.event.EventType;
import javafx.stage.Stage;

public class UpdateTitle extends Event {

    public static final EventType<UpdateTitle> eType = new EventType<>("UpdateTitle");

    private UpdateTitle(EventType<? extends Event> eventType) {
        super(eventType);
    }

    public static void fireFrom(Stage n) {
        n.fireEvent(new UpdateTitle(eType));
    }
}
