package org.bitbucket.brittlefoot.pop3.client;


class LoginInfo {

    public final String login;
    public final String pass;

    public LoginInfo(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }
}
