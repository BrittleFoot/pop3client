package org.bitbucket.brittlefoot.pop3.client;


public enum UseTls {
    YES,
    NO,
    AUTO
}
