package org.bitbucket.brittlefoot.pop3.client.events;

import javafx.event.Event;
import javafx.event.EventType;
import javafx.stage.Stage;

public class LoadLetterList extends Event {

    public static final EventType<LoadLetterList> eType = new EventType<>("LoadLetterList");

    private LoadLetterList(EventType<? extends Event> eventType) {
        super(eventType);
    }

    public static void fireFrom(Stage n) {
        n.fireEvent(new LoadLetterList(eType));
    }
}
