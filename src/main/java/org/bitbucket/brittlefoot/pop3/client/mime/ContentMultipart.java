package org.bitbucket.brittlefoot.pop3.client.mime;

import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.Collection;

public class ContentMultipart extends MimeContent {

    private final ArrayList<ContentNode> content;

    public ContentMultipart(MimeMessage msg) {
        super(msg);
        content = new ArrayList<>();
        String boundary = msg.contentType.args.getOrDefault("boundary", "").trim();
        if (boundary.isEmpty()) {
            content.add(new ContentNode(new TextField(msg.body), "WTF"));
            return;
        }

        if (boundary.startsWith("\"") && boundary.endsWith("\"")) {
            boundary = boundary.substring(1, boundary.length() - 1);
        }

        String normalBondary = "--" + boundary;
        String endBoundary = "--" + boundary + "--";

        StringBuilder builder = new StringBuilder();
        boolean started = false;
        for (String line : msg.body.split("\n")) {
            line = line.trim();
            if (line.equals(normalBondary) || line.equals(endBoundary)) {
                if (!started) {
                    started = true;
                }
                else {
                    MimeContent stored = MimeContent.get(builder.toString());
                    content.addAll(stored.getContent());
                    builder = new StringBuilder();
                    if (line.equals(endBoundary)) {
                        return;
                    }
                }
            }
            else if (started) {
                builder.append(line).append('\n');
            }

        }
    }

    @Override
    public Collection<ContentNode> getContent() {
        return content;
    }

}
