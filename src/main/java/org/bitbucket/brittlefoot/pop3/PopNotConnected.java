package org.bitbucket.brittlefoot.pop3;

public class PopNotConnected extends Exception {

    private final String msg;

    PopNotConnected(String reason) {
        msg = reason;
    }

    @Override
    public String getMessage() {
        return msg;
    }

}
