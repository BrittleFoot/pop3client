package org.bitbucket.brittlefoot.pop3;

import javafx.util.Pair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class Util {

    private final static char[] hexArray = "0123456789abcdef".toCharArray();
    private static final Predicate<String> pattern = Pattern.compile("^\\s").asPredicate();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * From rfc 2047: "encoded-word" is a sequence of printable ASCII characters
     * that begins with "=?", ends with "?=", and has two "?"s in between. It
     * specifies a character set and an encoding method, and also includes the
     * original text encoded as graphic ASCII characters, according to the rules
     * for that encoding method.
     *
     * @param encoded-word ~ "=?xxx?y?zzzzzzzz?="
     * @return human readable zzzzzzzz
     */
    public static String decodeEncodedWord(String encoded) throws UnsupportedEncodingException {
        String[] splitted = encoded.split("\\?");

        if (splitted.length != 5) {
            throw new UnsupportedEncodingException("Unexpected format: " + encoded);
        }

        String charset = splitted[1];
        String compressionType = splitted[2].toLowerCase();
        String message = splitted[3];

        if (!("b".equals(compressionType) || "q".equals(compressionType))) {
            throw new UnsupportedEncodingException("Unexpected compression type: " + compressionType);
        }

        return "b".equals(compressionType)
                ? decodeBase64(message, charset)
                : decodeQuotedPrintable(message, charset);
    }

    private static String decodeQuotedPrintable(String message, String charset)
            throws UnsupportedEncodingException {
        ArrayList<Byte> bbuffer = new ArrayList<>(message.length() / 2);

        StringBuilder out = new StringBuilder();
        StringBuilder sbuffer = new StringBuilder();

        int i = 0;
        while (i < message.length()) {
            Character curchar = message.charAt(i++);
            if (sbuffer.length() > 0) {
                sbuffer.append(curchar);

                if (sbuffer.length() == 3) {
                    bbuffer.add((byte) Integer.parseInt(sbuffer.substring(1), 16));
                    sbuffer = new StringBuilder();
                }

            }
            else {
                switch (curchar) {
                    case '=':
                        sbuffer.append(curchar);
                        break;
                    case '_':
                        bbuffer.add((byte) 0x20);
                        break;
                    default:
                        byte[] bytes = toPrimitiveBytes(bbuffer);
                        bbuffer.clear();
                        String bbuferRes = new String(bytes, charset);
                        out.append(bbuferRes).append(curchar);

                        bbuffer.clear();

                        break;
                }
            }

        }

        byte[] bytes = toPrimitiveBytes(bbuffer);
        bbuffer.clear();
        String bbuferRes = new String(bytes, charset);
        out.append(bbuferRes);

        return out.toString();

    }

    private static String decodeBase64(String message, String charset) throws UnsupportedEncodingException {
        return new String(Base64.getDecoder().decode(message), charset);
    }

    private static byte[] toPrimitiveBytes(ArrayList<Byte> al) {
        byte[] storedBytes = new byte[al.size()];
        for (int i = 0; i < al.size(); i++) {
            storedBytes[i] = al.get(i);
        }
        return storedBytes;
    }

    public static Pair<HashMap<String, String>, String> parseMime(String repr) {
        repr = repr.trim();


        HashMap<String, String> header = new HashMap<>();

        String lastkey = "";
        int len = 0;
        for (String line : repr.split("\n")) {
            len += line.length() + 1;
            if (line.trim().isEmpty()) {
                break;
            }

            if (pattern.test(line) && !lastkey.isEmpty()) {
                header.put(lastkey, header.get(lastkey) + " " + line.trim());
            }
            else if (line.contains(":")) {
                int sepindex = line.indexOf(":");
                lastkey = line.substring(0, sepindex);
                String value = line.substring(sepindex + 1);
                header.put(lastkey, value);
            }
        }

        String content = repr.substring(len).trim();

        return new Pair<>(header, content);
    }

}
