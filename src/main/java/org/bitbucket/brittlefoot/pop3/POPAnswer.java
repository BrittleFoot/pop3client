package org.bitbucket.brittlefoot.pop3;

public class POPAnswer {

    public final Boolean succ;
    public final String message;
    String additional;

    public POPAnswer(Boolean succ, String message) {
        this.succ = succ;
        this.message = message;
        this.additional = "";
    }

    public static POPAnswer parse(String line) {
        if (line == null) {
            return null;
        }

        Boolean succ = line.charAt(0) == '+';
        int hasargs = line.indexOf(' ') + 1;
        String message = "";
        if (hasargs != 0) {
            message = line.substring(hasargs);
        }

        return new POPAnswer(succ, message);
    }

    public String getAdditional() {
        return additional;
    }

}
