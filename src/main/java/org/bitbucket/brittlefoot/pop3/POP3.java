package org.bitbucket.brittlefoot.pop3;

import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class POP3 {

    private static final String CRLF = "\r\n";
    public final String domain;
    public final int port;
    private final Object lock = new Object();
    private Socket sock;
    private POPAnswer greeting = null;

    public POP3(String domain, int port) {
        this.domain = domain;
        this.port = port;
        this.sock = null;
    }

    private void putcmd(String cmd, String... args) throws PopNotConnected {
        if (args.length == 0) {
            send(String.format("%s%s", cmd, CRLF));
        }
        else {
            send(String.format("%s %s%s", cmd, String.join(" ", args), CRLF));
        }
    }

    private void send(String message) throws PopNotConnected {

        if (sock == null) {
            throw new PopNotConnected("connect() first");
        }

        try {
            OutputStream ostream = sock.getOutputStream();
            ostream.write(message.getBytes("ascii"));
            if (!message.startsWith("pass"))
                System.err.print("SEND>> " + message);
        }
        catch (IOException ex) {
            sock = null;
            greeting = null;
            ex.printStackTrace(System.err);
            throw new PopNotConnected(ex.getMessage());
        }
    }

    private POPAnswer getReply(boolean multiline) throws PopNotConnected {
        if (sock == null) {
            sock = null;
            greeting = null;
            throw new PopNotConnected("connect() first.");
        }

        try {
            BufferedReader istream = new BufferedReader(new InputStreamReader(sock.getInputStream(), "ascii"));
            String line = istream.readLine();
            System.err.println("REPLY< " + line);

            POPAnswer res = POPAnswer.parse(line);
            if (res == null) {
                return new POPAnswer(false, "Server diconnected");
            }

            if (!res.succ || !multiline) {
                return res;
            }

            StringBuilder builder = new StringBuilder();
            line = istream.readLine();
            while (!".".equals(line)) {
                builder.append(line).append("\n");
                line = istream.readLine();
            }

            res.additional = builder.toString();
            System.err.println("MULTILINE REPLY RECIVED " + res.additional.length());

            return res;

        }
        catch (IOException ex) {
            sock = null;
            greeting = null;
            throw new PopNotConnected(ex.getMessage());
        }
    }

    public POPAnswer connect() throws PopAlreadyConnected {
        return connect(port == 995);
    }

    public POPAnswer connect(boolean usessl) throws PopAlreadyConnected {
        if (sock != null) {
            throw new PopAlreadyConnected();
        }

        try {
            sock = usessl
                    ? SSLSocketFactory.getDefault().createSocket()
                    : new Socket();

            sock.setSoTimeout(3000);
            sock.connect(new InetSocketAddress(domain, port));
            greeting = getReply(false);
            return greeting;
        }
        catch (IOException |
                PopNotConnected ex) {
            sock = null;
            return new POPAnswer(false, "Error occured durning connection\n" + ex.getMessage());
        }
    }

    private POPAnswer docmd(boolean acceptMultiline, String cmd, String... args)
            throws PopNotConnected {
        synchronized (lock) {
            putcmd(cmd, args);
            return getReply(acceptMultiline);
        }
    }

    //------------------------------
    public POPAnswer user(String username) throws PopNotConnected {
        return docmd(false, "user", username);
    }

    public POPAnswer pass(String username) throws PopNotConnected {
        return docmd(false, "pass", username);
    }

    public void quit() throws PopNotConnected {
        POPAnswer qa = docmd(false, "quit");
        try {
            sock.close();
        }
        catch (IOException ignored) {
        }
        sock = null;
        greeting = null;
    }

    public POPAnswer list() throws PopNotConnected {
        return docmd(true, "list");
    }

    public POPAnswer retr(Integer target) throws PopNotConnected {
        return docmd(true, "retr", target.toString());
    }

    public POPAnswer dele(Integer target) throws PopNotConnected {
        return docmd(false, "dele", target.toString());
    }

    public POPAnswer top(Integer target, Integer prefferedLineCount) throws PopNotConnected {
        return docmd(true, "top", target.toString(), prefferedLineCount.toString());
    }

    public POPAnswer uidl() throws PopNotConnected {
        return docmd(true, "uidl");
    }

}
